using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpawner : MonoBehaviour
{
    public static PlayerSpawner instance;
    [SerializeField] GameObject[] playerShipPrefabs;
    public int selectedPlayerShip = 0;

    // Start is called before the first frame update
    void Start()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SpawnShip()
    {
        if (playerShipPrefabs.Length >0)
        {
            GameObject playerShip = Instantiate(playerShipPrefabs[selectedPlayerShip]);
            playerShip.transform.parent = transform;
            PlayerShipCamController.instance.SetPlayerShipTarget(playerShip.transform);
            EnemySpawner.instance.SetPlayerShipTarget(playerShip.transform);
        }
    }
}
