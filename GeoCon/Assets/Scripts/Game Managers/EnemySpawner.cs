using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField] GameObject[] enemyPrefabs;
    [SerializeField] Transform enemyHolder;
    public Transform playerShipTarget;
    //Determins the adjusted spawnrate and amounts
    [SerializeField] float scoreDivider = 100;
    //Bounds make sure enemies spawn away from the walls
    private Vector2 spawningLowerBounds = new Vector2(-13, -8);
    private Vector2 spawningUpperBounds = new Vector2(13, 8);
    private float spawnDistanceBuffer = 0.6f;
    [SerializeField] float spawnRate = 2;
    private float minSpawnRate = 1f;
    [SerializeField] int enemiesToSpawnAtATime = 2;
    private int maxEnemiesToSpawnAtATime = 4;
    public int currentEnemyCount;
    public static EnemySpawner instance;
    float deltaTimeBetweenSpawns;
    public bool gameStarted;

    private void Start()
    {
        if(instance == null)
        {
            instance = this;
        }
    }


    private void Update()
    {
        if (gameStarted == true)
        {
            EnemySpawningManager();
        }
    }
    //Driven by player score, as the player score increases enemys spawn more frequently and in bigger clusters
    //
    public void EnemySpawningManager()
    {
        
        deltaTimeBetweenSpawns += Time.deltaTime;
        if (deltaTimeBetweenSpawns > spawnRate)
        {
            deltaTimeBetweenSpawns = 0;
            for (int i = 0; i < enemiesToSpawnAtATime; i++)
            {
                int randomEnemyIndex = Random.Range(0, enemyPrefabs.Length);
                float randXPos = Random.Range(spawningLowerBounds.x,spawningUpperBounds.x);
                float randYPos = Random.Range(spawningLowerBounds.y,spawningUpperBounds.y);
                Vector3 randomPosition = new Vector3(randXPos, randYPos, 0);
                //Makes sure not to spawn on top of player
                if (Vector3.Distance(randomPosition,playerShipTarget.position)<spawnDistanceBuffer)
                {
                    randXPos += Random.Range(2, 4);
                    randYPos += Random.Range(2, 4);
                    randomPosition = new Vector3(randXPos, randYPos, 0);
                }
                SpawnEnemy(randomEnemyIndex, randomPosition);
            }
        }
    }

    public void UpdateSpawningParameters()
    {
        //Update spawning parameters based on player score
        float newSpawnRate = spawnRate / (ScoreManager.instance.playerScore / scoreDivider);
        if (newSpawnRate < spawnRate && newSpawnRate > minSpawnRate)
        {
            spawnRate = newSpawnRate;
        }

        int newEnemiesToSpawnAtATime = enemiesToSpawnAtATime * Mathf.RoundToInt(ScoreManager.instance.playerScore / scoreDivider) - 1;
        if (newEnemiesToSpawnAtATime > enemiesToSpawnAtATime && newEnemiesToSpawnAtATime < maxEnemiesToSpawnAtATime)
        {
            enemiesToSpawnAtATime = newEnemiesToSpawnAtATime;
        }
    }

    public void SpawnEnemy(int enemyPrefabIndex, Vector3 pos)
    {
        currentEnemyCount++;
        GameObject enemy = Instantiate(enemyPrefabs[enemyPrefabIndex], enemyHolder);
        enemy.transform.position = pos;
    }

    public void SetPlayerShipTarget(Transform playerShip)
    {
        playerShipTarget = playerShip;
    }

    public void DestroyAllEnemies()
    {
        foreach (Transform enemy in enemyHolder)
        {
            Destroy(enemy.gameObject);
        }

        currentEnemyCount = 0;
    }

    public void GameStarted()
    {
        gameStarted = true;
    }

    public void GameEnded()
    {
        gameStarted = false;
    }
}
