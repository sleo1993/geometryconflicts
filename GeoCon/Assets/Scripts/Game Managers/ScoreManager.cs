using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ScoreManager : MonoBehaviour
{
    public static ScoreManager instance;
    public int playerScore;
    public TextMeshProUGUI scoreUI;

    void Start()
    {
        if (instance== null)
        {
            instance = this;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void UpdatePlayerScore(int score)
    {
        if (score != 0)
        {
            playerScore += score;
        }
        else
        {
            playerScore = 0;
        }

        scoreUI.text = "SCORE: " + playerScore;

        EnemySpawner.instance.UpdateSpawningParameters();
    }
}
