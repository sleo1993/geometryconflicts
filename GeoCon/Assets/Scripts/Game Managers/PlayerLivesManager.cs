using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayerLivesManager : MonoBehaviour
{
    public static PlayerLivesManager instance;
    PlayerSpawner playerSpawner;
    int playerLives = 3;
    public TextMeshProUGUI livesCounterUI;
    public GameObject EndGameScreen;
    public TextMeshProUGUI finalScoreUI;

    // Start is called before the first frame update
    void Start()
    {
        if (instance == null)
        {
            instance = this;
        }

        //Visually sets player lives counter
        livesCounterUI.text = "LIVES: " + playerLives;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void RemovePlayerLife()
    {
        playerLives--;
        livesCounterUI.text = "LIVES: " + playerLives;
        if (playerLives == 0)
        {
            EndGame();
        }
        else
        {
            Respawn();
        }
    }

    //Shows end game UI showing player score, exit button, and return to main menu.
    private void EndGame()
    {
        
        EnemySpawner.instance.GameEnded();
        EndGameScreen.SetActive(true);
        finalScoreUI.text = ScoreManager.instance.playerScore.ToString();
        ScoreManager.instance.UpdatePlayerScore(0);
    }

    private void Respawn()
    {
        PlayerSpawner.instance.SpawnShip();
    }

    public void SetPlayerLive(int lives)
    {
        playerLives = lives;
        livesCounterUI.text = "LIVES: " + playerLives;
    }
}
