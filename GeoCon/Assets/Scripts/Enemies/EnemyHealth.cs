using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour
{
    [SerializeField] EnemyAI enemyAI;
    int health = 1;

    public void TakeDamage()
    {
        health--;

        if (health <= 0)
            EnemyDestroyed();
    }

    private void EnemyDestroyed()
    {
        //Removes enemy from enemy count
        EnemySpawner.instance.currentEnemyCount--;
        ScoreManager.instance.UpdatePlayerScore(enemyAI.GetScore());
        Destroy(gameObject);
    }
}
