using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAI : MonoBehaviour
{
    [SerializeField] protected int score;
    [SerializeField] protected float movmentSpeed;
    [SerializeField] protected Transform target;
    protected Vector2 movmentLowerBounds = new Vector2(-15, -10);
    protected Vector2 movmentUpperBounds = new Vector2(15, 10);

    private void Start()
    {
        AssignTarget();
    }
    protected void AssignTarget()
    {
        target = EnemySpawner.instance.playerShipTarget;
    }
    public int GetScore()
    {
        return score;
    }

}
