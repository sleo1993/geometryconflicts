using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Eratic : EnemyAI
{
    float deltaTimeResetEraticDirection = 10f;
    float timeToResetEraticDirection = 1f;
    float minSpeedMultiplier = 0.45f;
    float maxSpeedMultiplier = 1.25f;
    float speedMultiplier = 1;
    Vector3 eraticDirection;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        EraticMovmentTowardShip();
    }

    public void EraticMovmentTowardShip()
    {

        if (target != null)
        {
            deltaTimeResetEraticDirection += Time.deltaTime;
            if (deltaTimeResetEraticDirection > timeToResetEraticDirection)
            {
                deltaTimeResetEraticDirection = 0;
                timeToResetEraticDirection = Random.Range(0.5f, 2f);
                Vector3 targetDirection = target.position - transform.position;
                float randXDir = Random.Range(-1f, 1f);
                float randYDir = Random.Range(-1f, 1f);
                Vector3 randomDirectionFromTarget = new Vector3(randXDir, randYDir, 0);
                eraticDirection = targetDirection += randomDirectionFromTarget;
                speedMultiplier = Random.Range(minSpeedMultiplier, maxSpeedMultiplier);
            }
            if (transform.position.x < movmentLowerBounds.x || transform.position.x > movmentUpperBounds.x ||
                transform.position.y < movmentLowerBounds.y || transform.position.y > movmentUpperBounds.y)
            {
                //Targets ship directly to get back on track
                eraticDirection = target.position - transform.position;
            }

            transform.Translate(eraticDirection.normalized * movmentSpeed* speedMultiplier*Time.deltaTime, Space.World);
        }
        else
        {
            AssignTarget();
        }
    }
}
