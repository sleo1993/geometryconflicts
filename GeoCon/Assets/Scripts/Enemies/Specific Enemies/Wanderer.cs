using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wanderer : EnemyAI
{
    [SerializeField] float rotationSpeed;
    float deltaTimeResetWanderDirection =2f;
    float timeToResetWanderDirection = 1f;
    Vector3 wanderDirection;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        WanderTowardShip();
        Spin();
    }

    public void WanderTowardShip()
    {
        if (target != null)
        {
            deltaTimeResetWanderDirection += Time.deltaTime;
            if (deltaTimeResetWanderDirection >timeToResetWanderDirection)
            {
                deltaTimeResetWanderDirection = 0;
                Vector3 targetDirection = target.position - transform.position;
                float randXDir = Random.Range(-1f, 1f);
                float randYDir = Random.Range(-1f, 1f);
                Vector3 randomDirectionFromTarget = new Vector3(randXDir,randYDir,0);
                wanderDirection = targetDirection += randomDirectionFromTarget;
            }
            if (transform.position.x < movmentLowerBounds.x || transform.position.x>movmentUpperBounds.x || 
                transform.position.y < movmentLowerBounds.y || transform.position.y > movmentUpperBounds.y)
            {
                //Targets ship directly to get back on track
                wanderDirection = target.position - transform.position;
            }

            transform.Translate(wanderDirection.normalized*movmentSpeed*Time.deltaTime, Space.World);
        }
        else
        {
            AssignTarget();
        }
    }

    public void Spin()
    {
        transform.Rotate(new Vector3(0, 0, rotationSpeed*Time.deltaTime*10));
    }
}
