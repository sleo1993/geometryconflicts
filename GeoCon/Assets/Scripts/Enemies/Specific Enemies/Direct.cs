using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Direct : EnemyAI
{
    // Update is called once per frame
    void Update()
    {
        DirectMoveTowardShip();
    }

    private void DirectMoveTowardShip()
    {
        if (target!=null)
        {
            Vector3 targetDirection = target.position - transform.position;
            transform.Translate(targetDirection.normalized * movmentSpeed*Time.deltaTime);
        }
        else
        {
            AssignTarget();
        }

    }
}
