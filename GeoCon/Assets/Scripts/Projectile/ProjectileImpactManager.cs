using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileImpactManager : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Map Bounds"))
        {
            Destroy(gameObject.transform.parent.gameObject);
        }
        else if (collision.CompareTag("Enemy"))
        {
            //Parent of the collider is the main enemy gameobject with the various controllers
            collision.GetComponentInParent<EnemyHealth>().TakeDamage();
            Destroy(gameObject.transform.parent.gameObject);
        }
    }
}
