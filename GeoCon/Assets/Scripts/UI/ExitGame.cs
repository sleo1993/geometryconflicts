using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitGame : MonoBehaviour
{

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            ExitGameButton();
        }
    }
    public void ExitGameButton()
    {
        //if (Application.isEditor == true)
        //{
        //    UnityEditor.EditorApplication.isPlaying = false;
        //}
        //else
        {
            Application.Quit();
        }
    }
}
