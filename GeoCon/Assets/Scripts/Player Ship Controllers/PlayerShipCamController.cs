using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShipCamController : MonoBehaviour
{
    public static PlayerShipCamController instance;
    public bool gameStarted;
    [SerializeField] float camMoveSpeed;
    Transform playerShipTarget;
    // Start is called before the first frame update
    void Start()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    private void Update()
    {
        if (gameStarted == true)
        {
            FollowShip();
        }
    }

    public void ReturnToStartMenuPosition()
    {
        transform.position = Vector2.zero;
    }

    public void SetPlayerShipTarget(Transform playerShip)
    {
        playerShipTarget = playerShip;
    }

    //Follows movment of the ship at a scaled speed slower than the ship
    private void FollowShip()
    {
        if (playerShipTarget != null)
        {
            Vector3 targetPosition = new Vector3(playerShipTarget.position.x, playerShipTarget.position.y, transform.position.z);
            transform.position = Vector3.Lerp(transform.position, targetPosition, camMoveSpeed * Time.deltaTime);
        }
    }

    public void GameStarted()
    {
        gameStarted = true;
    }
}
