using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerShipHealth : MonoBehaviour
{
    int health = 1;
    bool sheilded = false;
    float spawnSheildDuration = 1.5f;
    [SerializeField] GameObject sheildVisualEffect;

    public void Start()
    {
        //Start Sheilded On Start
        StartCoroutine(SheildHandler(spawnSheildDuration));
    }

    public void TakeDamage()
    {
        if (sheilded == false)
            health--;

        if (health <= 0)
            PlayerShipDestroyed();
    }

    //Plays ship destruction animation and sound
    //Removes life from player lives
    //Destroys all enemis
    //When live is removed, "PlayersLiveManager" will check to either respawn player or end game
    private void PlayerShipDestroyed()
    {
        EnemySpawner.instance.DestroyAllEnemies();

        PlayerLivesManager.instance.RemovePlayerLife();
        Destroy(gameObject);
    }

    IEnumerator SheildHandler(float duration)
    {
        sheilded = true;
        //Activates Sheild Visual Effect
        sheildVisualEffect.SetActive(true);
        yield return new WaitForSeconds(duration);
        sheilded = false;
        //Deactivates sheild visual effect
        sheildVisualEffect.SetActive(false);
    }

}
