using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShipWeaponController : MonoBehaviour
{
    [SerializeField] Transform projectileHolder;
    [SerializeField] GameObject weaponProjectilePrefab;
    [SerializeField] float weaponDamage;
    [SerializeField] float weaponFireRate; //Measured in seconds between shots
    float deltaTimeBetweenShots;
    [SerializeField] float distanceFromShipToSpawnProjectile;
    Vector2 playerShipFiringDirection = new Vector2(0, 0);
    bool firing = false;

    private void Start()
    {
        //Iniffecient. As long as GameObject.Find is not called frequently it is ok.
        projectileHolder = GameObject.Find("Projectile Holder").transform;
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.RightArrow))
        {
            int up = 0, down = 0, left = 0, right = 0;
            if (Input.GetKey(KeyCode.UpArrow))
                up = 1;
            if (Input.GetKey(KeyCode.LeftArrow))
                left = -1;
            if (Input.GetKey(KeyCode.DownArrow))
                down = -1;
            if (Input.GetKey(KeyCode.RightArrow))
                right = 1;
            //Takes the vector sum of Arrow keys and calculates direction. 
            //Up = +Y, Down = -Y, Left = -X, Right = +X 
            playerShipFiringDirection = new Vector2(left + right, up + down);
            firing = true;
        }
        else
        {
            playerShipFiringDirection = new Vector2(0, 0);
            firing = false;
        }
        deltaTimeBetweenShots += Time.deltaTime;
        if (firing == true)
        {
            if (deltaTimeBetweenShots > weaponFireRate)
            {
                deltaTimeBetweenShots = 0;
                //Spawns projectile, will upgrade to an object pooler if time allows
                GameObject projectile = Instantiate(weaponProjectilePrefab);
                projectile.transform.parent = projectileHolder;
                projectile.transform.position = playerShipFiringDirection * distanceFromShipToSpawnProjectile + (Vector2)transform.position;

                float targetAngleChange = Vector2.SignedAngle(projectile.transform.up, playerShipFiringDirection * distanceFromShipToSpawnProjectile);
                projectile.transform.Rotate(new Vector3(0, 0, targetAngleChange));
            }
        }
    }
}
