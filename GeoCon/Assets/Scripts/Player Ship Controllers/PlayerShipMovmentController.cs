using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShipMovmentController : MonoBehaviour
{
    //Only visible when moving
    [SerializeField] GameObject shipExaustEffect;
    [SerializeField] float shipSpeed;
    [SerializeField] float smoothRotationSpeed;
    [SerializeField] float shipSizeBuffer;
    private Vector2 movmentLowerBounds = new Vector2(-15,-10);
    private Vector2 movmentUpperBounds = new Vector2(15,10);
    Vector2 playerShipDirection = new Vector2(0,0);
    bool moving;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        int w = 0, s = 0, a = 0, d = 0;
        //Handles exaust visual effect
        if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.D))
        {
            if (Input.GetKey(KeyCode.W))
                w = 1;
            if (Input.GetKey(KeyCode.A))
                a = -1;
            if (Input.GetKey(KeyCode.S))
                s = -1;
            if (Input.GetKey(KeyCode.D))
                d = 1;
            //Takes the vector sum of WASD and calculates direction. 
            //W = +Y, S = -Y, A = -X, D = +X 
            playerShipDirection = new Vector2(a+d, w + s);
            moving = true;
        }
        else
        {
            moving = false;
            playerShipDirection = new Vector2(0, 0);
        }
        if (moving == true)
        {
            if (shipExaustEffect.activeSelf == false)
                shipExaustEffect.SetActive(true);

            //Determins if new ship location is within perdefined ship bounds
            Vector2 targetMovment = playerShipDirection * shipSpeed*Time.deltaTime;
            Vector2 targetMovmentForAngleTarget = new Vector2();
            targetMovmentForAngleTarget = targetMovment; //Needed reference so angle calculation does not confuse updated target movment when at bounds
            Vector2 newTargertPosition = targetMovment + (Vector2)transform.position;
            if (newTargertPosition.x-shipSizeBuffer < movmentLowerBounds.x)
            {
                targetMovment.x = movmentLowerBounds.x-transform.position.x+shipSizeBuffer;
            }
            else if(newTargertPosition.x+shipSizeBuffer > movmentUpperBounds.x)
            {
                targetMovment.x = transform.position.x - movmentUpperBounds.x + shipSizeBuffer;
            }
            if (newTargertPosition.y - shipSizeBuffer < movmentLowerBounds.y)
            {
                targetMovment.y = movmentLowerBounds.y - transform.position.y + shipSizeBuffer;
            }
            else if (newTargertPosition.y + shipSizeBuffer > movmentUpperBounds.y)
            {
                targetMovment.y = transform.position.y - movmentUpperBounds.y + shipSizeBuffer;
            }


            //Handles Movment
            //Takes calculated direction and applies specified speed
            transform.Translate(targetMovment, Space.World);
            float targetAngleChange = Vector2.SignedAngle(transform.up, targetMovmentForAngleTarget);
            //Smooths Angle Change
            float smoothAngle = Mathf.Lerp(0, targetAngleChange, smoothRotationSpeed);
            transform.Rotate(new Vector3(0, 0, smoothAngle*Time.deltaTime*10));
        }
        else
        {
            if (shipExaustEffect.activeSelf == true)
                shipExaustEffect.SetActive(false);
        }
    }
}
